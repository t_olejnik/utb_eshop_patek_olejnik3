﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Domain.Services.Carts
{
    public interface ICartService
    {
        CartItem AddToCart(int productID, int amount, string userTrackingCode);
        void RemoveFromCart(int productID, string userTrackingCode);
        IList<CartItem> GetCartItems(string userTrackingCode);
    }
}
