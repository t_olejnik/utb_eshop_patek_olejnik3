﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Services.Orders
{
    public interface IOrderService
    {
        void CreateOrder(int userID, string userTrackingCode);
    }
}
