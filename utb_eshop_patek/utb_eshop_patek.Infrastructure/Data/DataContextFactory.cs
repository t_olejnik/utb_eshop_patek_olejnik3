﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Infrastructure.Data
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseSqlServer("Server=databaze.fai.utb.cz;"
                + "Database=A12345_A5PWT;"
                + "User ID=A12345;"
                + "Password=A12345A12345;"
                + "persist security info=True;"
                + "multipleactiveResultsets=True;");
            return new DataContext(builder.Options);
        }
    }
}
