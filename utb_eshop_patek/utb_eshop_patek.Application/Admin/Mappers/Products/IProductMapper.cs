﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Application.Admin.Mappers.Products
{
    public interface IProductMapper
    {
        ProductViewModel GetViewModelFromEntity(Product entity);
        Product GetEntityFromViewModel(ProductViewModel viewModel);
        IList<ProductViewModel> GetViewModelsFromEntities(IList<Product> entities);
    }
}
