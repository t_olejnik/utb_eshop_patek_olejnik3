﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Services.Orders;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Orders
{
    public class OrderService : IOrderApplicationService
    {
        private readonly IOrderService _orderService;

        public OrderService(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public void CreateOrder(int userID, string userTrackingCode)
        {
            _orderService.CreateOrder(userID, userTrackingCode);
        }
    }
}
