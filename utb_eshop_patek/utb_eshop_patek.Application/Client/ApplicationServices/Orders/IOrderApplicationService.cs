﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Orders
{
    public interface IOrderApplicationService
    {
        void CreateOrder(int userID, string userTrackingCode);
    }
}
