﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Carts;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Application.Client.Mappers
{
    public interface ICartMapper
    {
        IList<CartItemViewModel> GetViewModelsFromEntities(IList<CartItem> entities);
    }
}
