﻿namespace utb_eshop_patek.Application.Client.ViewModels.Carts
{
    public class CartItemViewModel
    {
        public string ProductName { get; set; }
        public string ImageUrl { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public int ProductID { get; set; }
    }
}